<?php
session_start();
include ('db.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Chat</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://kit.fontawesome.com/644c1f6431.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="chat_assets/style.css">
	<script type="text/javascript">
		$(document).ready(function(){
			setTimeout(function () {
				$(".chat_body").scrollTop($(" .chat_body").get(0).scrollHeight);
			}, 200);
		});
	</script>
</head>
<body>
	<?php

	if(isset($_GET['campaignid'])){
		$_SESSION['campaign'] = $_GET['campaignid'];
		
		$model= mysqli_query($conn,"SELECT * FROM `items` WHERE `campaignid` ='".$_GET['campaignid']."'");
		while($row =mysqli_fetch_assoc($model)){
			$_SESSION['model'] = $row['id'];

			if (empty($_SESSION['sender'])) {
				$_SESSION['sender'] = uniqid();				
			}
			?>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-3 live_agent">
							<div class="col-md-12 live_header">
								<img src="http://litechatbot.com/chatbot/assets/uploads/<?php echo $row['profile'];?>">		
							</div>
							<div class="col-md-12 live_body">
								<div class="col-md-5">
									<h6>Name : </h6>
								</div>
								<div class="col-md-5">
									<h6><?php echo $row['name'];?></h6>
								</div>
								<div class="col-md-1 status">
									<img src="http://litechatbot.com/chat/uploads/online.jpg">
								</div>
								<div class="col-md-6">
									<h6>Age : </h6>
								</div>
								<div class="col-md-6">
									<h6><?php echo $row['age'];?></h6>
								</div>

								<div class="col-md-6">
									<h6>City : </h6>
								</div>
								<div id="city"></div>
								<script type="text/javascript">
									$(document).ready(function(){
										setInterval(function(){
											$("#city").load("location.php").fadeIn("slow");
										}, 1000);
										$.ajaxSetup({ cache: false });
									});
								</script>
							</div>					
						</div>
						<div class="col-md-9 chat_window">
							<div class="col-md-12 chat_header">
								<div class="col-md-1">
									<img src="http://litechatbot.com/chatbot/assets/uploads/<?php echo $row['profile'];?>">
								</div>
								<div class="col-md-4">
									<h4><?php echo $row['name'];?></h4>
								</div>
							</div>
							<div class="col-md-12 live_chat"></div>
							<script type="text/javascript">
								$(document).ready(function(){
					              // $(".chat_body").scrollTop($(" .chat_body").get(0).scrollHeight);
					              //$(".chat_body").animate({ scrollTop: $(' .chat_body').prop("scrollHeight")}, 10);
					          });
					      </script>

					      <div class="col-md-12 chat_form">
					      	<form id="submit_form" enctype="multipart/form-data">
					      		<div class="col-md-1 attachment_btn">
					      			<label for="file">
					      				<i class="fas fa-paperclip"></i>
					      				<input type="file" id="file" style="display: none" name="attachment" accept="image/gif,image/jpeg,image/jpg,image/png" multiple="" data-original-title="upload photos">
					      			</label>
					      		</div>
					      		<div class="col-md-10">
					      			<input class="form-control" type="text" name="message">
					      		</div>
					      		<div class="col-md-1 chat_btn">
					      			<div>
					      				<button type="submit" name="submit" id="btnsend"><i class="fas fa-paper-plane"></i></button>
					      			</div>
					      		</div>
					      	</form>
					      </div>
					  </div>
					</div>
				</div>
			</div>
			<script type="text/javascript">
				$(document).ready(function (e) {
					$("#submit_form").on('submit',(function(e) {
						e.preventDefault();
						var fdata = new FormData(this);
						$.ajax({
							url: "action.php",   
							type: "POST",             
							data: fdata,   
							contentType: false,          
							cache: false,         
							processData:false,        
							success: function(data){
				            // alert(data);
				            // $("#message").html(data);
				            $("#submit_form")[0].reset();    
				        }         
				    });
				   }));
				});
			</script>
			<script type="text/javascript">
				$(document).ready(function () {					
					setInterval(function(){
						$.ajax({
							url: "chatbody.php",       
							success: function(data){
								$(".live_chat").html(data);
								$(".chat_body").scrollTop($(" .chat_body").get(0).scrollHeight);   
							}         
						});
					},2000);
				});
			</script>
			<?php
		}
	}
	?>
</body>
</html>