-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 08, 2020 at 03:47 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chatbot`
--

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `campaign_id` int(11) NOT NULL,
  `sender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `script` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `replay_script` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `replay_attachment` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `campaign_id`, `sender`, `receiver`, `script`, `replay_script`, `attachment`, `replay_attachment`, `created_at`, `updated_at`) VALUES
(17, 4, '4_4_5f7ef2a70e870', '4', 'hi', 'hello', '', NULL, '2020-10-08 11:06:44', '2020-10-08 12:51:13'),
(18, 4, '4_4_5f7ef2a70e870', '4', 'how are you?', '', '', NULL, '2020-10-08 11:06:52', '2020-10-08 11:06:52'),
(19, 4, '4_4_5f7ef2a70e870', '4', 'why yo wont replay?', '', '', NULL, '2020-10-08 11:07:08', '2020-10-08 11:07:08'),
(20, 4, '4_4_5f7f028916fa4', '4', 'ok fine...', '', '', NULL, '2020-10-08 12:14:27', '2020-10-08 12:14:27'),
(21, 4, '4_4_5f7f02a7028f5', '4', 'ok', '', '_1602159281.jpg', NULL, '2020-10-08 12:14:41', '2020-10-08 12:14:41'),
(22, 4, '4', '4_4_5f7f02a7028f5', 'how are you?', 'i am fine and you?', '', NULL, '2020-10-08 12:22:52', '2020-10-08 13:24:20'),
(23, 4, '4_4_5f7f065243b21', '4', 'qwertyui', '', '', NULL, '2020-10-08 12:30:31', '2020-10-08 12:30:31'),
(24, 4, '4_4_5f7f065243b21', '4', 'ok', 'bye', '', NULL, '2020-10-08 12:30:39', '2020-10-08 13:18:35'),
(25, 4, '4_4_5f7f13070d96e', '4', 'thanks for your replay....', NULL, '', NULL, '2020-10-08 13:24:39', '2020-10-08 13:24:39'),
(26, 4, '4_4_5f7f15e497549', '4', 'thank', NULL, '', NULL, '2020-10-08 13:37:12', '2020-10-08 13:37:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
