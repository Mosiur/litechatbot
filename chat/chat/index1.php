<?php
session_start();
include ('db.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Chat</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://kit.fontawesome.com/644c1f6431.js" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <style type="text/css">
    body{
      min-height: 600px;
      width: 100%;
    }
    .col-md-1,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-md-10,.col-md-11,.col-md-12{
      float: left;
    }
    .col-md-3{
      background: #2C3E50;
      color: #FFFFFF;

    }
    .live_agent{
      height: 600px;
    }
    .live_header{
      background: #2C3E50;
      color: #FFFFFF;
      margin-top: 15px;
    }
    .live_header img{
      height: 40px;
      width: 40px;
      border-radius: 25px;
    }
    .live_header h6{
      margin-top: 7px;
    }
    .live_agent .status img{
      height: 10px;
      width: 10px;
    }
    .col-md-9{
      background: #F5F5F5;

    }
    .chat_window{
      height: 600px;
    }
    .chat_header{
      background: #F5F5F5;
      margin-top: 9px;
      margin-bottom: 9px;
    }
    .chat_header img{
      height: 60px;
      width: 60px;
      border-radius: 25px;
    }
    .chat_header h4{
      margin-top: 14px;
    }
    .chat_body{
      background: #E6EAEA;
      height: 474px;
      overflow: scroll;
    }
    .chat_body p{
      margin-top: 6px;
      overflow: hidden;
    }
    .receiver{
      margin-top: 5px;
      margin-bottom: 5px;
    }
    .receiver .message p{
      background: #435F7A;
      color: #FFFFFF;
      border-top-left-radius: 6px;
      border-top-right-radius: 6px;
      border-bottom-right-radius: 0px;
      padding: 5px;
    }
    .receiver img{
      height: 30px;
      width: 30px;
      border-radius: 25px;
    }
    .receiver .attachment img{
      height: 120px;
      width: 150px;
      float: left;
    }
    .sender p{
      text-align: right;
    }
    .sender{
      margin-top: 5px;
      margin-bottom: 5px;
    }
    .sender .attachment img{
      height: 120px;
      width: 150px;
      float: right;
    }
    .sender .col-md-8{
      float: right;
    }
    .sender .message p{
      background: #1db1b1;
      color: #000000;
      border-top-left-radius: 8px;
      border-top-right-radius: 10px;
      border-bottom-left-radius: 5px;
      float: right;
      padding: 3px;
    }
    .chat_form{
      margin-top: 5px;
      margin-bottom: 5px;
      background: #E6EAEA;
    }
    .chat_form .attachment_btn{
      margin-top: 10px;
    }
    .chat_form .chat_btn{
      margin-top: 5px;
    }
  </style>
   <script type="text/javascript">
    $(document).ready(function(){
      setInterval(function(){
        $(".row").load(" .row").fadeIn("slow");
      }, 1000);
      $.ajaxSetup({ cache: false });
    });
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
      $(document).scrollTop(0);
    });
  </script>

<?php
$_SESSION['campaign'] = 1;
$_SESSION['model'] =  1;
$_SESSION['sender'] = $_SESSION['campaign'].'_'.$_SESSION['model'].'_'.uniqid();
?>
</head>
<body>
  <div class="container">
    <div class="row">

      <?php
      $sender_data = array();
      $receiver_data = array();
      $data = mysqli_query($conn,"SELECT * FROM `chat` WHERE `receiver` = 1 || `sender`=1 ORDER BY `created_at` ASC");
      while ($row = mysqli_fetch_assoc($data)) {
        $model =mysqli_fetch_assoc(mysqli_query($conn,"SELECT `id`,`name`, `age`, `profile` FROM `items` WHERE `id` ='".$row['receiver']."' AND `campaignid` ='".$row['campaign_id']."'"));

        if($model['id'] == $row['receiver']){

          $sender_name =$row['sender'];
          $sender_profile ='';
          $sender_data[] = ['message'=>$row['script'],'replay_message'=>$row['replay_script'],'attachment'=>$row['attachment'],'replay_attachment'=>$row['replay_attachment']];

          $receiver_name = $model['name'];
          $receiver_profile =$model['profile'];

          
        }
      }

      // echo "<pre>";
      // print_r($sender_data);

      // echo "+++++++++++++++++++++++++++++++++";

      // echo "<pre>";
      // print_r($receiver_data);
      ?>
      <div class="col-md-12">
        <div class="col-md-3 live_agent">
          <div class="col-md-12 live_header">
            <div class="col-md-3">
              <img src="http://localhost/MOSIUR/trust_it/chatbot/assets/uploads/<?php echo $receiver_profile;?>">
            </div>
            <div class="col-md-7">
              <h6><?php echo $receiver_name;?></h6>
            </div>
            <div class="col-md-2 status">
              <img src="http://localhost/MOSIUR/trust_it/chatbot/assets/uploads//chat/online.jpg">
            </div>
          </div>
        </div>
        <div class="col-md-9 chat_window">
          <div class="col-md-12 chat_header">
            <div class="col-md-1">
              <img src="http://localhost/MOSIUR/trust_it/chatbot/assets/uploads/<?php echo $receiver_profile;?>">
            </div>
            <div class="col-md-4">
              <h4><?php echo $receiver_name;?></h4>
            </div>
          </div>
          <div class="col-md-12 chat_body">

            <!--  sender -->
            <?php
            if ($sender_data != NULL) {
              foreach ($sender_data as $key => $row) {?>
                <?php
                if($row['message'] != NULL){
                  ?>
                  <div class="col-md-12 sender">
                    <div class="col-md-8 message">
                      <div class="col-md-12"><p><?php echo $row['message']; ?></p></div>
                      <div class="col-md-12 attachment">
                        <?php
                        if($row['attachment'] != NULL){?>
                          <img src="uploads/<?php echo $row['attachment'];?>">
                          <?php
                        }
                        ?>
                      </div>
                    </div>
                  </div>
                  <?php
                }
                if($row['replay_message'] != NULL){
                  ?>
                  <div class="col-md-12 receiver">
                    <div class="col-md-1 profile_pic">
                      <img src="http://localhost/MOSIUR/trust_it/chatbot/assets/uploads/<?php echo $receiver_profile;?>">
                    </div>
                    <div class="col-md-8 message">
                      <div class="col-md-12"><p><?php echo $row['replay_message']; ?></p></div>
                      <div class="col-md-12 attachment">
                        <?php
                        if($row['replay_attachment'] != NULL){?>
                          <img src="uploads/<?php echo $row['replay_attachment'];?>">
                          <?php
                        }
                        ?>
                      </div>
                    </div>
                  </div>
                  <?php
                }
              }
            }
            ?>
          </div>
          <div class="col-md-12 chat_form">
            <form id="submit_form" enctype="multipart/form-data">
              <div class="col-md-1 attachment_btn">
                <label for="file">
                  <i class="fas fa-paperclip"></i>
                  <input type="file" id="file" style="display: none" name="attachment" accept="image/gif,image/jpeg,image/jpg,image/png" multiple="" data-original-title="upload photos">
                </label>
              </div>
              <div class="col-md-10">
                <input class="form-control" type="text" name="message">
              </div>
              <div class="col-md-1 chat_btn">
                <div>
                  <button type="submit" name="submit" id="btnsend"><i class="fas fa-paper-plane"></i></button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    $(document).ready(function (e) {
      $("#submit_form").on('submit',(function(e) {
        e.preventDefault();

        var fdata = new FormData(this);
        $.ajax({
          url: "action.php",   
          type: "POST",             
          data: fdata,   
          contentType: false,          
          cache: false,         
          processData:false,        
          success: function(data){
            // alert(data);
            // $("#message").html(data);
            $("#submit_form")[0].reset();    
          }         
        });
      }));
    });
  </script>
</body>
</html>