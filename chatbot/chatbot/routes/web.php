<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


 //Clear route cache:
 Route::get('/route-cache', function() {
     $exitCode = Artisan::call('route:cache');
     return 'Routes cache cleared';
 });

 //Clear config cache:
 Route::get('/config-cache', function() {
     $exitCode = Artisan::call('config:cache');
     return 'Config cache cleared';
 }); 

// Clear application cache:
 Route::get('/clear-cache', function() {
     $exitCode = Artisan::call('cache:clear');
     return 'Application cache cleared';
 });

 // Clear view cache:
 Route::get('/view-clear', function() {
     $exitCode = Artisan::call('view:clear');
     return 'View cache cleared';
 });
Route::get('/getusersysteminfo','visitorController@getusersysteminfo');
Route::get('/truckuser','visitorController@truckuser');
 
Route::get('/admin', function () { return view('pages/login');});
Route::post('/signin','auth@index')->name('signin');
Route::group(['middleware'=>['userAuthentication']],function(){

	Route::get('/','dashboard@index');

	// Conversation
	Route::get('/visitor','conversationController@index');
	Route::get('/delete_visitor/{id}','conversationController@delete')->name('delete_visitor');
	Route::get('/conversation','conversationController@conversation');
	Route::get('/delete_conversation','conversationController@delete')->name('delete_conversation');
	Route::get('/view_conversation','conversationController@view_conversation');

	//Camping
	Route::get('/campaign', 'campaignController@index');
	Route::post('/addcampign', 'campaignController@insert');
	Route::get('/edit_campaign', 'campaignController@edit');
	Route::post('/updt_campaign', 'campaignController@updt_campaign');
	Route::get('/delete_campaign/{id}', 'campaignController@delete')->name('delete_campaign');


	//report
	Route::get('/report','reportController@index');
	Route::post('/searchreport','reportController@searchreport');

	//Script
	Route::get('/script','scriptController@index');
	Route::post('/addscript','scriptController@insert');
	Route::get('/edit_script','scriptController@edit');
	Route::post('/updatescript','scriptController@update');
	Route::get('/delete_script/{id}','scriptController@delete')->name('delete_script');

	//Keyword
	Route::get('/keyword','keywordController@index');
	Route::post('/addkeyword','keywordController@insert');
	Route::get('/edit_keyword','keywordController@edit');
	Route::post('/updatekeyword','keywordController@update');
	Route::get('/delete_keyword/{id}','keywordController@delete')->name('delete_keyword');

	// message Link
	Route::get('/link','linkController@index');
	Route::post('/addlink','linkController@insert');
	Route::get('/edit_link','linkController@edit');
	Route::post('/updatelink','linkController@update');
	Route::get('/delete_link/{id}','linkController@delete')->name('delete_link');

	//chat link
	Route::get('/chatlink','chatlinkController@index');
	Route::post('/addchatlink','chatlinkController@insert');
	Route::get('/edit_chatlink','chatlinkController@edit');
	Route::post('/updatechatlink','chatlinkController@update');
	Route::get('/delete_chatlink/{id}','chatlinkController@delete')->name('delete_chatlink');

	//Model
	Route::get('/model','modelController@index');
	Route::post('/addmodel','modelController@insert');
	Route::get('/edit_model','modelController@edit');
	Route::post('/updatemodel','modelController@update');
	Route::get('/delete_model/{id}','modelController@delete')->name('delete_model');

	//gallery
	Route::get('/gallery','galleryController@index');
	Route::post('/addgallery','galleryController@insert');
	Route::get('/delete_gallery/{id}','galleryController@delete')->name('delete_gallery');



	//User management
	Route::get('/manageuser','userController@alluser');
	Route::post('/adduser','userController@insert');
	Route::get('/edit_user','userController@edituser');
	Route::post('/updateuser','userController@updateuser');
	Route::get('/delete_user/{id}','userController@delete')->name('delete_user');

	//reset System
	Route::get('/reset_system','resetController@reset_system');
	Route::post('/reset','resetController@resetsystem');


	//chat
	Route::get('/chat','chatController@index');
	Route::get('/oneFunction','chatController@oneFunction');
	Route::get('/anotherFunction','chatController@anotherFunction');


});


// Administration
Route::group(['middleware'=>['adminAccess']],function(){
	Route::get('/dashboard','dashboard@index1')->name('/dashboard');
	
	//User management
	Route::get('/manageuser','userController@alluser');
	Route::post('/adduser','userController@insert');
	Route::get('/edit_user','userController@edituser');
	Route::post('/updateuser','userController@updateuser');
	Route::get('/delete_user/{id}','userController@delete')->name('delete_user');

	//Reset System
	Route::get('/reset_system','resetController@reset_system');
	Route::post('/reset','resetController@resetsystem');

});
Route::get('/cron/{campaignid}','chatController@conversation');
Route::get('/logout', function () {
	Session::flush();
	return redirect('/admin')->with('status', 'You are looged out...!');
});