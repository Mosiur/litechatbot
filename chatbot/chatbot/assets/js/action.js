
function edit_campaign(id){
	alert(id);
	var token = $('meta[name="csrf_token"]').attr('content'),
	$.ajax({
		url:'{{url::to('edit_campaign')}}',
		method:"GET",
		data:{
			id:id,
			_token: token,
		},
		success:function(data){
			$('#campaigndetails').html(data);
			$('#editcampaign').modal('show');
		}
	});
}
