@extends('master')
@section('content')
<!--Main Content-->
<div class="main-content px-0 app-content">
	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">
		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Dashboard</h3>
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
			</ol>
		</div>
		<!--Page Header-->
		
		<!-- Row -->
		<div class="row row-sm">

			<div class="col-xl-3 col-sm-6">
				<div class="card mg-b-20">
					<div class="card-body">
						<div class="d-flex mb-3">
							<div class="d-flex">
								<div class="cryp-icon bg-warning mr-2">
									<i class="cf cf-xrp"></i>
								</div>
								<p class="mb-0 mt-1 font-weight-semibold">Camping</p>
							</div>
						</div>
						<div class="d-flex mt-2 mb-1">
							<div>
								<h3 class="mb-1">{{$campaigns}}</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-xl-3 col-sm-6">
				<div class="card mg-b-20">
					<div class="card-body">
						<div class="d-flex mb-3">
							<div class="d-flex">
								<div class="cryp-icon bg-primary mr-2">
									<i class="cf cf-btc"></i>
								</div>
								<p class="mb-0  mt-1 font-weight-semibold">Todays visitor</p>
							</div>
							<div class="ml-auto">
								<p class="mt-1 tx-12 mb-0 text-success"><i class="ion-arrow-up-c mr-1"></i>+0.25%</p>
							</div>
						</div>
						<div class="d-flex mt-2 mb-1">
							<div>
								<h3 class="mb-1">{{$todaysvisitor}}</h3>
							</div>
						</div>
						<div class="progress ht-5 mt-2 mb-1">
							<div aria-valuemax="100" aria-valuemin="0" aria-valuenow="70" class="progress-bar bg-primary wd-70p" role="progressbar"></div>
						</div>
						<small class="mb-0 text-muted">Conversation Rate<span class="float-right text-muted">70%</span></small>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-sm-6">
				<div class="card mg-b-20">
					<div class="card-body">
						<div class="d-flex mb-3">
							<div class="d-flex">
								<div class="cryp-icon bg-secondary mr-2">
									<i class="cf cf-eth"></i>
								</div>
								<p class="mb-0  mt-1 font-weight-semibold">Total Visitor</p>
							</div>
						</div>
						<div class="d-flex mt-2 mb-1">
							<div>
								<h3 class="mb-1">{{$visitor}}</h3>
							</div>
						</div>
						<div class="progress ht-5 mt-2 mb-1">
							<div aria-valuemax="100" aria-valuemin="0" aria-valuenow="70" class="progress-bar bg-secondary wd-50p" role="progressbar"></div>
						</div>
						<small class="mb-0 text-muted">Conversation Rate<span class="float-right text-muted">50%</span></small>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-sm-6">
				<div class="card mg-b-20">
					<div class="card-body">
						<div class="d-flex mb-3">
							<div class="d-flex">
								<div class="cryp-icon bg-info mr-2">
									<i class="cf cf-ltc"></i>
								</div>
								<p class="mb-0 mt-1 font-weight-semibold">Total Script</p>
							</div>
						</div>
						<div class="d-flex mt-2 mb-1">
							<div>
								<h3 class="mb-1">{{$scripts}}</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-sm-6">
				<div class="card mg-b-20">
					<div class="card-body">
						<div class="d-flex mb-3">
							<div class="d-flex">
								<div class="cryp-icon bg-info mr-2">
									<i class="cf cf-ltc"></i>
								</div>
								<p class="mb-0 mt-1 font-weight-semibold">Total Keyword</p>
							</div>
						</div>
						<div class="d-flex mt-2 mb-1">
							<div>
								<h3 class="mb-1">{{$keyword}}</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-sm-6">
				<div class="card mg-b-20">
					<div class="card-body">
						<div class="d-flex mb-3">
							<div class="d-flex">
								<div class="cryp-icon bg-info mr-2">
									<i class="cf cf-ltc"></i>
								</div>
								<p class="mb-0 mt-1 font-weight-semibold">Total Modal</p>
							</div>
						</div>
						<div class="d-flex mt-2 mb-1">
							<div>
								<h3 class="mb-1">{{$models}}</h3>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xl-3 col-sm-6">
				<div class="card mg-b-20">
					<div class="card-body">
						<div class="d-flex mb-3">
							<div class="d-flex">
								<div class="cryp-icon bg-info mr-2">
									<i class="cf cf-ltc"></i>
								</div>
								<p class="mb-0 mt-1 font-weight-semibold">Total Message Link</p>
							</div>
						</div>
						<div class="d-flex mt-2 mb-1">
							<div>
								<h3 class="mb-1">{{$messagelink}}</h3>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xl-3 col-sm-6">
				<div class="card mg-b-20">
					<div class="card-body">
						<div class="d-flex mb-3">
							<div class="d-flex">
								<div class="cryp-icon bg-info mr-2">
									<i class="cf cf-ltc"></i>
								</div>
								<p class="mb-0 mt-1 font-weight-semibold">Total Chat Link</p>
							</div>
						</div>
						<div class="d-flex mt-2 mb-1">
							<div>
								<h3 class="mb-1">{{$chatlink}}</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<!--/Row -->
	</div><!--Main Content Container-->
</div>
<!--Main Content-->
@endsection