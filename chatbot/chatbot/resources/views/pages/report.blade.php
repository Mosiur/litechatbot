@extends('master')
@section('content')
<!--Main Content-->
<div class="main-content px-0 app-content">
	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">
		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Report</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Report</li>
			</ol>
		</div>
		<!--Page Header-->
		<div class="page-header" style="margin: 0px !important;">

			<input type="hidden" name="_token" id="csrf" value="{{Session::token()}}">
			<div class="col-md-10">
				<div class="row row-sm mg-b-20">

					<div class="col-lg-4 mg-t-20 mg-lg-t-0">
						<p class="mg-b-10">Camping</p>
						<select class="form-control select2" name="camping" id="camping" required="required">
							<option label="Choose one"></option>
							<?php
							foreach ($data as $key => $value) {
								echo '<option value="'.$value->id.'">'.$value->name.'</option>';
							}
							?>
						</select>
					</div><!-- col-4 -->
					<div class="col-lg-4 mg-t-20 mg-lg-t-0">
						<p class="mg-b-10">From</p>
						<input name="from" class="form-control select2" type="date" id="from" required="required">
					</div><!-- col-4 -->
					<div class="col-lg-4 mg-t-20 mg-lg-t-0">
						<p class="mg-b-10">To</p>
						<input name="to" class="form-control select2" type="date" id="to" required="required">
					</div><!-- col-4 -->
				</div>
			</div>
			<div class="col-md-2">
				<div class="row row-xs align-items-center mg-b-20">
					<div class="col-md-8 mg-t-5 mg-md-t-0">
						<button class="btn btn-main-primary pd-x-10 mg-r-5 mg-t-25" type="submit" id="butsave" >Search</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Row -->
		<div class="row" id="report_result">
		</div>
		<!-- /Row -->
	</div><!--Main Content Container-->
</div>
<!--Main Content-->
@endsection