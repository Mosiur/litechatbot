@extends('master')
@section('content')
<form method="POST" action="{{url('/addlink')}}">
	@csrf
	<div class="modal" id="addlink">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content tx-size-lg">
				<div class="modal-header">
					<h6 class="modal-title">Add Link</h6>
				</div>
				<div class="modal-body">
					<div class="row row-xs align-items-center mg-b-20">
						<div class="col-md-3">
							<label class="form-label mg-b-0">Link:</label>
						</div>
						<div class="col-md-9 mg-t-5 mg-md-t-0">
							<input name="link" class="form-control" placeholder="Enter Link address" type="text">
						</div>
					</div>
					<div class="row row-xs align-items-center mg-b-20">
						<div class="col-md-3"></div>
						<div class="col-md-9 mg-t-5 mg-md-t-0">
							<div class="col-md-2" style="float: left; margin: 0px; padding: 0px; display: block;">
								<button class="btn btn-main-primary pd-x-10 mg-r-5 mg-t-5" type="submit">Add Link</button>
							</div>
							<div class="col-md-3" style="float: left; margin: 0px; padding: 0px; display: block;">
								<button class="btn btn-dark pd-x-25 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<!-- MODAL ALERT MESSAGE -->
<form method="POST" action="{{url('/updatelink')}}">
	@csrf
	<div class="modal" id="editlink">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Update Link</h6>
				</div>
				<div class="modal-body" id="linkdetails">
				</div>
			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">
	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">
		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Link</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Link</li>
			</ol>
		</div>
		<!--Page Header-->
		<?php 
		if(Session::get('user')['privillage'] == 1000){?>
			<div class="page-header" style="margin: 0px !important;">
				<div class="col-md-10"></div>
				<div class="col-md-2">
					<button class="btn btn-info" data-toggle="modal" data-target="#addlink" style="float: right; border-radius: 10px;">Add Link</button>
				</div>
			</div>
			<?php
		}
		?>
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Message Link
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>SL</th>
										<th>Link</th>
										<th colspan="2">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$count=0;
									foreach ($link as $key => $value) {?>
										<tr>
											<th scope="row"><?php echo ++$count;?></th>
											<td><?php echo $value->link;?></td>
											<td>
												<a class="text-info" style="font-size: 22px;" onclick="edit_link('{{$value->id}}')"><i class="si si-note" data-toggle="tooltip" title="Edit"></i>
												</a>
											</td>
											<td>
												<a class="text-danger" style="font-size: 22px;" href="{{ route('delete_link', ['id' => $value->id])}}"><i class="ti-trash" data-toggle="tooltip" title data-original-title="delete"></i>
												</a>
											</td>
										</tr>
										<?php
									}
									?>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div><!--Main Content Container-->
</div>
<!--Main Content-->
@endsection