@extends('master')
@section('content')

<!--Main Content-->
<div class="main-content px-0 app-content">
	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">
		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Reset System</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
				<?php
				echo "<script>
				setTimeout(function () {
					window.location.replace('http://localhost/MOSIUR/trust_it/chatbot/reset_system')
					}, 3000);
					</script>";
					?>
				</div>
				@endif
				@if (session('failed'))
				<div class="alert alert-danger">
					{{ session('failed') }}
					<?php
					echo "<script>
					setTimeout(function () {
						window.location.replace('http://localhost/MOSIUR/trust_it/chatbot/reset_system')
						}, 3000);
						</script>";
						?>
					</div>
					@endif
					<ol class="breadcrumb mb-0">
						<li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
						<li class="breadcrumb-item active" aria-current="page">Reset System</li>
					</ol>
				</div>
				<!--Page Header-->
				<!--Row-->
				<div class="row row-sm">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-body">
								<form action="{{url('/reset')}}" method="POST">
									@csrf
									@foreach($table as $row)
									<?php if($row->Tables_in_chatbot=='items' || $row->Tables_in_chatbot=='conversation' || $row->Tables_in_chatbot=='scripts' || $row->Tables_in_chatbot=='galleries' || $row->Tables_in_chatbot=='items'|| $row->Tables_in_chatbot=='keywords'){?>
										<div class="row row-xs align-items-center mg-b-20">
											<div class="col-lg-3">
												<label class="ckbox">
													<input type="checkbox" name="<?php echo $row->Tables_in_chatbot;?>">
													<span><?php echo $row->Tables_in_chatbot;?></span>
												</label>
											</div>
										</div>
										<?php
									}
									?>
									@endforeach
									<div class="row row-xs align-items-center mg-b-20">
										<div class="col-md-8 mg-t-5 mg-md-t-0">
											<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Reset System</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!--/Row -->
			</div>
		</div>
		<!--Main Content-->
		@endsection