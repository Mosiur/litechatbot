@extends('master')
@section('content')
<form method="POST" action="{{url('/addmodel')}}" enctype="multipart/form-data">
	@csrf
	<div class="modal" id="addmodel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content tx-size-lg">
				<div class="modal-header">
					<h6 class="modal-title">Add Model</h6>
				</div>
				<div class="modal-body">
					<div class="row row-xs align-items-center mg-b-20">
						<div class="col-md-3">
							<label class="form-label mg-b-0">Name:</label>
						</div>
						<div class="col-md-9 mg-t-5 mg-md-t-0">
							<input name="name" class="form-control" placeholder="Enter Model Name" type="text" required>
						</div>
					</div>
					<div class="row row-xs align-items-center mg-b-20">
						<div class="col-md-3">
							<label class="form-label mg-b-0">age:</label>
						</div>
						<div class="col-md-9 mg-t-5 mg-md-t-0">
							<input name="age" class="form-control" placeholder="Enter Model age" type="number" required>
						</div>
					</div>
					<div class="row row-xs align-items-center mg-b-20">
						<div class="col-md-3">
							<label class="form-label mg-b-0">Profile Picture:</label>
						</div>
						<div class="col-md-9 mg-t-5 mg-md-t-0">
							<input  type="file" name="profile" class="form-control" required>
						</div>
					</div>
					<div class="row row-xs align-items-center mg-b-20">
						<div class="col-md-3"></div>
						<div class="col-md-9 mg-t-5 mg-md-t-0">
							<div class="col-md-3" style="float: left; margin: 0px; padding: 0px; display: block;">
								<button class="btn btn-main-primary pd-x-10 mg-r-5 mg-t-5" type="submit">Add Model</button>
							</div>
							<div class="col-md-2" style="float: left; margin: 0px; padding: 0px; display: block;">
								<button class="btn btn-dark pd-x-25 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<!-- MODAL ALERT MESSAGE -->
<form method="POST" action="{{url('/updatemodel')}}" enctype="multipart/form-data">
	@csrf
	<div class="modal" id="editmodel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Update Model</h6>
				</div>
				<div class="modal-body" id="modeldetails">
				</div>
			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">
	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">
		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Model</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Model</li>
			</ol>
		</div>
		<!--Page Header-->
		<div class="page-header" style="margin: 0px !important;">
			<div class="col-md-10"></div>
			<div class="col-md-2">
				<button class="btn btn-info" data-toggle="modal" data-target="#addmodel" style="float: right; border-radius: 10px;">Add Model</button>
			</div>
		</div>
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Model
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>SL</th>
										<th>Name</th>
										<th>Age</th>
										<th>Profile</th>
										<th colspan="2">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$count=0;
									foreach ($model as $key => $value) {?>
										<tr>
											<th scope="row"><?php echo ++$count;?></th>
											<td><?php echo $value->name;?></td>
											<td><?php echo $value->age;?></td>
											<td><img style="height: 60px;width:60px; float: left;"  src="<?php echo 'assets/uploads/' . $value->profile;?>"></td>
											<td>
												<a class="text-info" style="font-size: 22px;" onclick="edit_model('{{$value->id}}')"><i class="si si-note" data-toggle="tooltip" title="Edit"></i>
												</a>
											</td>
											<td>
												<a class="text-danger" style="font-size: 22px;" href="{{ route('delete_model', ['id' => $value->id])}}"><i class="ti-trash" data-toggle="tooltip" title data-original-title="delete"></i>
												</a>
											</td>
										</tr>
										<?php
									}
									?>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div><!--Main Content Container-->
</div>
<!--Main Content-->
@endsection