@extends('master')
@section('content')
<!--Main Content-->
<div class="main-content px-0 app-content">

	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">

		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Visitor</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Visitor</li>
			</ol>
		</div>
		<!--Page Header-->
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Visitor
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>Campign</th>
										<th>IP</th>
										<th>ISO Code</th>
										<th>City</th>
										<th>State</th>
										<th>State Name</th>
										<th>Postal Code</th>
										<th>Lat</th>
										<th>Lon</th>
										<th>Timezone</th>
										<th>Continent</th>
										<th>Currency</th>
										<th>Visited At</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($visitors as $row)
									<tr>
										<th scope="row">{{$row->campaignid}}</th>
										<td>{{$row->ip}}</td>
										<td>{{$row->iso_code}}</td>
										<td>{{$row->country}}</td>
										<td>{{$row->city}}</td>
										<td>{{$row->state}}</td>
										<td>{{$row->state_name}}</td>
										<td>{{$row->postal_code}}</td>
										<td>{{$row->lat}}</td>
										<td>{{$row->lon}}</td>
										<td>{{$row->timezone}}</td>
										<td>{{$row->continent}}</td>
										<td>{{$row->currency}}</td>
										<td>{{$row->created_at}}</td>
										<!-- <td>
											<a class="text-warning" style="font-weight: bold;font-size: 22px;" onclick="edit_conversation('{{$row->id}}')"><i class="si si-note" data-toggle="tooltip" title="Edit"></i>
											</a>
										</td> -->
										<td>
											<a class="text-danger" style="font-size: 22px;" href="{{ route('delete_visitor', ['id' => $row->id])}}"><i class="ti-trash" data-toggle="tooltip" title data-original-title="delete"></i></i>
											</a>
											</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div><!--Main Content Container-->
</div>
<!--Main Content-->
@endsection