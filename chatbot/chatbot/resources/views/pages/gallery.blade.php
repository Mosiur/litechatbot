@extends('master')
@section('content')
<form method="POST" action="{{url('/addgallery')}}" enctype="multipart/form-data">
	@csrf
	<div class="modal" id="addgallery">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content tx-size-lg">
				<div class="modal-header">
					<h6 class="modal-title">Add Image</h6>
				</div>
				<div class="modal-body">
					<div class="row row-xs align-items-center mg-b-20">
						<div class="col-md-3">
							<label class="form-label mg-b-0">Picture:</label>
						</div>
						<div class="col-md-9 mg-t-5 mg-md-t-0">
							<input  type="file" name="img[]" class="form-control" multiple required>
						</div>
					</div>
					<div class="row row-xs align-items-center mg-b-20">
						<div class="col-md-3"></div>
						<div class="col-md-9 mg-t-5 mg-md-t-0">
							<div class="col-md-2" style="float: left; margin: 0px; padding: 0px; display: block;">
								<button class="btn btn-main-primary pd-x-20 mg-r-5 mg-t-5" type="submit">Save</button>
							</div>
							<div class="col-md-2" style="float: left; margin: 0px; padding: 0px; display: block;">
								<button class="btn btn-dark pd-x-25 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<!-- MODAL ALERT MESSAGE -->
<form method="POST" action="{{url('/updategallery')}}">
	@csrf
	<div class="modal" id="gallery">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Update Link</h6>
				</div>
				<div class="modal-body" id="details">
				</div>
			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">
	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">
		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Gallery</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Gallery</li>
			</ol>
		</div>
		<!--Page Header-->
		<div class="page-header" style="margin: 0px !important;">
			<div class="col-md-10"></div>
			<div class="col-md-2">
				<button class="btn btn-info" data-toggle="modal" data-target="#addgallery" style="float: right; border-radius: 10px;">Add Image</button>
			</div>
		</div>
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="tab-pane" id="gallery">
							<div class="card-body">
								<div class="row row-sm">
									<?php
									foreach ($gallery as $key => $value) {?>
										<div class="col-6 col-md-3">
											<img style="position: relative;" alt="<?php echo $value->name;?>" class="img-thumbnail" src="<?php echo $value->link;?>">
											<a class="text-danger" style="font-size: 22px;" href="{{ route('delete_gallery', ['id' => $value->id])}}">
											<i style="float: right; font-size: 20px; font-weight: bold; position: absolute; margin-left: -21px;" class="fe fe-x text-danger" data-toggle="tooltip" title="fe fe-x"></i></a>
										</div>
										<?php
									}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div><!--Main Content Container-->
</div>
<!--Main Content-->
@endsection