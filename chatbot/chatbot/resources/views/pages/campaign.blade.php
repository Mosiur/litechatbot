@extends('master')
@section('content')
<form method="POST" action="{{url('/addcampign')}}">
	@csrf
	<div class="modal" id="addcampaign">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content tx-size-sm">
				<div class="modal-header">
					<h6 class="modal-title">Add Campaign</h6>
				</div>
				<div class="modal-body">
					<div class="pd-30 pd-sm-40 bg-light">
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">Camping Name</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<input name="name" class="form-control" placeholder="Enter Camping Name" type="text">
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4"></div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<div class="col-md-7" style="float: left; margin: 0px; padding: 0px; display: block;">
									<button class="btn btn-main-primary pd-x-25 mg-r-5 mg-t-5" type="submit">Save</button>
								</div>
								<div class="col-md-4" style="float: left; margin: 0px; padding: 0px; display: block;">
									<button class="btn btn-dark pd-x-25 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<!-- <form method="POST" action="{{url('/exitingcampaign')}}">
	@csrf
	<div class="modal" id="exitingcampaign">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content tx-size-sm">
				<div class="modal-header">
					<h6 class="modal-title">Add Sub-Campaign</h6>
				</div>
				<div class="modal-body">
					<div class="pd-30 pd-sm-40 bg-light">
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">Sub Camping Name</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<input name="campaign_name" class="form-control" placeholder="Enter Camping Name" type="text">
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">Existing Camping</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<select class="form-control" name="flag" required="required">
									<?php
									foreach ($campaigns as $key => $value) {
										echo "<option value='".$value->id."'>".$value->name."</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4"></div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<div class="col-md-5" style="float: left; margin: 0px; padding: 0px; display: block;">
									<button class="btn btn-main-primary pd-x-25 mg-r-5 mg-t-5" type="submit">Save</button>
								</div>
								<div class="col-md-4" style="float: left; margin: 0px; padding: 0px; display: block;">
									<button class="btn btn-dark pd-x-25 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form> -->
<!-- MODAL ALERT MESSAGE -->
<form method="POST" action="{{url('/updt_campaign')}}">
	@csrf
	<div class="modal" id="editcampaign">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content tx-size-sm">

				<div class="modal-body tx-center pd-y-20 pd-x-20" id="campaigndetails">
				</div>

			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">

	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">

		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Camping</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Camping</li>
			</ol>
		</div>
		<!--Page Header-->
		<?php 
		if(Session::get('user')['privillage'] == 1000){?>
			<div class="page-header" style="margin: 0px !important;">
				<div class="col-md-7"></div>
				<div class="col-md-2">
					<button class="btn btn-info" data-toggle="modal" data-target="#addcampaign" style="float: right; border-radius: 10px;">Create Campign</button>
				</div>
				<!-- <div class="col-md-2">
					<button class="btn btn-info" data-toggle="modal" data-target="#exitingcampaign" style="float: right; border-radius: 10px;">With Existing Campign</button>
				</div> -->
			</div>
			<?php
		}
		?>
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Camping
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>Campaign</th>
										<th>Script</th>
										<th>Keyword</th>
										<th>Model Name</th>
										<th>Message Link</th>
										<th>Chat Link</th>
										<th colspan="2">Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($campaigns as $campaignrow)
									<tr>
										<td>{{$campaignrow->name}}</td>
										<td>{{$scripts}}</td>
										<td>{{$keyword}}</td>
										<td>
											<ol>
												<?php
												foreach ($models as $key => $row) {
													echo "<li>".$row->name."</li>";
												}
												?>
											</ol>
										</td>
										<td>
											<ol>
												<?php
												foreach ($messagelink as $key => $row) {
													echo "<li><a href='".$row->link."' target='blank'>".$row->link."</a></li>";
												}
												?>
											</ol>
										</td>
										<td>
											<ol>
												<?php
												foreach ($chatlink as $key => $row) {
													echo "<li><a href='".$row->chat_link."' target='blank'>".$row->chat_link."</a></li>";
												}
												?>
											</ol>
										</td>
										<td>
											<a class="text-warning" style="font-weight: bold;font-size: 22px;" onclick="edit_campaign('{{$campaignrow->id}}')"><i class="si si-note" data-toggle="tooltip" title="Edit"></i>
											</a>
										</td>
										<td>
											<a class="text-danger" style="font-size: 22px;" href="{{ route('delete_campaign', ['id' => $campaignrow->id])}}"><i class="ti-trash" data-toggle="tooltip" title data-original-title="delete"></i></i>
											</a>
											</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div><!--Main Content Container-->
</div>
<!--Main Content-->
@endsection