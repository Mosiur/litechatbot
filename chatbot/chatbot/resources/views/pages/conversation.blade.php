@extends('master')
@section('content')
<link href="{{asset('assets/style.css')}}" rel="stylesheet">
<!-- MODAL ALERT MESSAGE -->
<div class="modal" id="chatwindow">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content modal-content-demo">
			<div class="modal-header">
				<h6 class="modal-title">Conversation</h6>
			</div>
			<div class="modal-body" id="chatbody">
			</div>
		</div>
	</div>
</div>
<!--Main Content-->
<div class="main-content px-0 app-content">

	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">

		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Conversation</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Conversation</li>
			</ol>
		</div>
		<!--Page Header-->
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Conversation
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>User</th>
										<th>Campaing</th>
										<th>City</th>
										<th>Country</th>
										<th>Script</th>
										<th>Create Date</th>
										<th colspan="2">Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($userdata as $row)
									<tr>
										<th scope="row">{{$row['user']}}</th>
										<td>{{$row['campaign']}}</td>
										<td>{{$row['city']}}</td>
										<td>{{$row['Country']}}</td>
										<td>{{$row['script']}}</td>
										<td>{{$row['created_at']}}</td>
										<td>
											<a class="text-warning" style="font-weight: bold;font-size: 22px;" onclick="view_conversation('<?php echo $row['user'];?>')"><i class="far fa-eye" data-toggle="tooltip" title="View"></i>
											</a>
										</td>
										<td>
											<a class="text-danger" style="font-size: 22px;" href="{{ route('delete_conversation', ['userid' => $row['user']])}}"><i class="ti-trash" data-toggle="tooltip" title data-original-title="delete"></i></i>
											</a>
											</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div><!--Main Content Container-->
</div>
<!--Main Content-->
@endsection