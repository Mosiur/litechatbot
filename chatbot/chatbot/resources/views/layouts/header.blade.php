<!-- main-header -->
<div class="header-main">
	<div class="main-header">
		<div class="container-fluid">
			<!--logo-->
			<div class="main-header-left">
				<div class="hor-logo">
					<?php
					if(Session::get('user')['privillage']!=1000){?>
						<a class="main-logo desktop-logo" href="{{url('/')}}"><img src="{{asset('assets/img/brand/logo-white.png')}}" alt="logo"></a>
						<a class="main-logo mobile-logo" href="{{url('/')}}"><img src="{{asset('assets/img/brand/favicon-white.png')}}" alt="logo"></a>
						<?php
					}else{?>
						<a class="main-logo desktop-logo" href="{{route('/dashboard')}}"><img src="{{asset('assets/img/brand/logo-white.png')}}" alt="logo"></a>
						<a class="main-logo mobile-logo" href="{{route('/dashboard')}}"><img src="{{asset('assets/img/brand/favicon-white.png')}}" alt="logo"></a>

						<?php
					}
					?>
				</div>
				<!-- sidebar-toggle-->
				<div class="app-sidebar__toggle" data-toggle="sidebar">
					<a class="open-toggle" href="#"><i class="header-icons" data-eva="menu-outline"></i></a>
					<a class="close-toggle" href="#"><i class="header-icons" data-eva="close-outline"></i></a>
				</div>
				<!-- /Sidebar-toggle-->
			</div>
			<div class="main-header-right ml-auto">
				<div class="dropdown main-header-search mobile-search">
					<a class="new header-link" href="">
						<i class="header-icons" data-eva="search-outline"></i>
					</a>
					<div class="dropdown-menu">
						<div class="p-2 main-form-search">
							<input class="form-control" placeholder="Search here..." type="search">
							<button class="btn"><i class="fe fe-search"></i></button>
						</div>
					</div>
				</div>
				<div class="header-formsearch mr-2">
					<input class="form-control" placeholder="Search" type="search">
					<button class="btn"><i class="fe fe-search"></i></button>
				</div>
				<div class="main-header-fullscreen fullscreen-button">
					<a href="#" class="header-link">
						<i class="header-icons" data-eva="expand-outline"></i>
					</a>
				</div>
				<div class="dropdown main-header-message">
					<a class="new header-link" href="">
						<i class="header-icons" data-eva="email-outline"></i>
						<span class="badge bg-success nav-link-badge">3</span>
					</a>
					<div class="dropdown-menu">
						<div class="p-3 border-bottom text-center">
							<h6 class="main-notification-title">Messages</h6>
						</div>
						<div class="main-notification-list">
							<div class="media new">
								<div class="main-img-user"><img alt="" src="{{asset('assets/img/users/male/avatar.png')}}"></div>
								<div class="media-body">
									<p> <strong>Jack Wright</strong> Congratulatation your template awesome</p><span>Jan 15 12:32pm</span>
								</div>
							</div>
						</div>
						<div class="dropdown-footer">
							<a href="">View All Messages</a>
						</div>
					</div>
				</div>
				<div class="dropdown main-header-notification d-none d-md-flex">
					<a class="new header-link" href="">
						<i class="header-icons" data-eva="bell-outline"></i>
						<span class="pulse bg-danger"></span>
					</a>
					<div class="dropdown-menu">
						<div class="p-3 border-bottom text-center">
							<h6 class="main-notification-title">Notifications</h6>
						</div>
						<div class="main-notification-list">
							<a href="#" class="dropdown-item d-flex">
								<div class="text-primary tx-18 mr-3 ">
									<i class="fe fe-mail"></i>
								</div>
								<div>
									<h6 class="mb-1">Commented on your post.</h6>
									<div class="small text-muted">3 hours ago</div>
								</div>
							</a>
						</div>
						<div class="dropdown-footer">
							<a href="">View All Notifications</a>
						</div>
					</div>
				</div>
				<div class="dropdown main-profile-menu">
					<a class="main-img-user" href="">
						<img alt="" src="{{asset('assets/img/users/male/avatar.png')}}">
					</a>
					<div class="dropdown-menu">
						<div class="main-header-profile">
							<h6>{{Session::get('user')['name']}}</h6>
							<?php
							if(Session::get('user')['privillage']==1){
								echo "<span>User</span>";
							}elseif (Session::get('user')['privillage']==2) {
								echo "<span>Sub-Admin</span>";
							}elseif(Session::get('user')['privillage']==1000){
								echo "<span>Admin</span>";
							}else{
								echo "<span>Unknown</span>";
							}
							?>
						</div>
						<a class="dropdown-item" href="#"><i class="si si-user"></i> Profile</a>
						<a class="dropdown-item" href="#"><i class="si si-envelope-open"></i> Inbox</a>
						<!-- <a class="dropdown-item" href="#"><i class="si si-calendar"></i> Activity</a>
						<a class="dropdown-item" href="#"><i class="si si-bubbles"></i> Chat</a>
						<a class="dropdown-item" href="#"><i class="si si-settings"></i> Settings</a> -->
						<a class="dropdown-item" href="{{url('/logout')}}"><i class="si si-power"></i> Logo Out</a>
					</div>
				</div>
				<div class="main-header-sidebar-notification">
					<a href="#" class="header-link" data-toggle="sidebar-right" data-target=".sidebar-right">
						<i class="header-icons" data-eva="options-2-outline"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!--/main-header-->