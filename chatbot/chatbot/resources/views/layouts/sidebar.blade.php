<!-- App Sidebar -->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
	<div class="col-lg-12" style="margin-top: 12px !important;">
		<div class="main-profile-overview">
			<div class="main-img-user profile-user">
				<img alt="" src="{{asset('assets/img/users/male/avatar.png')}}">
				<a href="JavaScript:void(0);" class="fas fa-camera profile-edit"></a>
			</div>
			<div id="time"></div>
		</div><!-- main-profile-overview -->
	</div>
	<ul class="side-menu">
		<?php
		if(Session::get('user')['privillage']){?>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/')}}"><i class="side-menu__icon fas fa-tv"></i><span class="side-menu__label">Dashboard</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/campaign')}}"><i class="side-menu__icon la la-bullhorn"></i><span class="side-menu__label">Campaign</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/conversation')}}"><i class="side-menu__icon si si-bubbles" ></i><span class="side-menu__label">Conversation</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/visitor')}}"><i class="side-menu__icon si si-location-pin" ></i><span class="side-menu__label">Visitor</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/report')}}"><i class="side-menu__icon fe fe-file-text"></i><span class="side-menu__label">Report</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/script')}}"><i class="side-menu__icon si si-book-open"></i><span class="side-menu__label">Script</span></a>
			</li>
			<!-- <li class="slide">
				<a class="side-menu__item" href="{{url('/keyword')}}"><i class="side-menu__icon si si-film"></i><span class="side-menu__label">Keyword</span></a>
			</li> -->
			<li class="slide">
				<a class="side-menu__item" href="{{url('/link')}}"><i class="side-menu__icon si si-envelope-letter"></i><span class="side-menu__label"> Message Link</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/chatlink')}}"><i class="side-menu__icon fe fe-link"></i><span class="side-menu__label"> Chat Link</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/model')}}"><i class="side-menu__icon si si-people"></i><span class="side-menu__label">Model</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/gallery')}}"><i class="side-menu__icon fe fe-image"></i><span class="side-menu__label">Gallery</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" target="blank" href="{{url('/chat')}}"><i class="side-menu__icon si si-bubble"></i><span class="side-menu__label">Chat</span></a>
			</li>
			<?php
			if(Session::get('user')['privillage']==1000){?>
				<li class="slide">
					<a class="side-menu__item" href="{{url('/manageuser')}}"><i class="side-menu__icon fe fe-users"></i><span class="side-menu__label">Manage User</span></a>
				</li>
				<li class="slide">
					<a class="side-menu__item" href="{{url('/reset_system')}}"><i class="side-menu__icon fas fa-redo"></i><span class="side-menu__label">Reset System</span></a>
				</li>
				<?php
			}
		}
		?>
	</ul>
</aside>
<!--/App Sidebar