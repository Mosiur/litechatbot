<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\gallery;
use DB;
use Session;
use File;
use Image;

class galleryController extends Controller
{
	public function index(){
		$campaignid=Session::get('user')['campaignid'];
        $gallery=gallery::where('campaignid','=',$campaignid)->get();
		return view('pages.gallery',['gallery'=>$gallery]);
	}

	public function insert(Request $request){

		$campaignid=Session::get('user')['campaignid'];		
		$img = $request->file('img');
		if ($request->hasfile('img')) {
			foreach ($img as $key => $image) {
				
		    	$extention=$image->getClientOriginalExtension();//File Extention
		    	$newimage_name=rand().time().'.'.$extention;
		    	$image_resize = Image::make($image->getRealPath());   
		    	$image_resize->resize(255,291);
		    	$image_resize->save('assets/uploads/'.$newimage_name);

		    	$gallery = new gallery;
		    	$gallery->campaignid = $campaignid;
		    	$gallery->name = $newimage_name;
		    	$gallery->link = 'assets/uploads/'.$newimage_name;
		    	$affected=$gallery->save();
		    }
		    if($affected){
		    	return redirect()->back()->with('success', 'Record Added successfully!');
		    }else{
		    	return redirect()->back()->with('failed', 'Failed  to Add Record!');
		    }
		}else{
			return redirect()->back()->with('failed', 'Please Select image!');
		}
	}

	public function delete($id){
        $res=gallery::find($id);
		if (File::exists("assets/uploads/".$res->name)) { 
            unlink("assets/uploads/".$res->name);
        }
		if($res->delete()){
			return redirect()->back()->with('success', 'Record Deleted successfully!');
		}else{
			return redirect()->back()->with('failed', 'Failed  to Delete Record!');
		}
    }
}
