<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class dashboard extends Controller
{
    public function autoload(){
        $campaignid=Session::get('user')['campaignid'];
        $campaigns = DB::table('campaign')->where('id','=',$campaignid)->get();
        return view('layouts.autoload',['campaigns'=>$campaigns]);

    }
    public function index(){
    	$today = date('Y-m-d H:m:s');
    	$campaignid=Session::get('user')['campaignid'];
		$campaign = DB::table('campaign')->where('id','=',$campaignid)->count();
		$script   = DB::table('scripts')->where('campaignid','=',$campaignid)->count();
		$keyword  = DB::table('keywords')->where('campaignid','=',$campaignid)->count();
		$models    = DB::table('items')->where('campaignid','=',$campaignid)->count();
		$messagelink = DB::table('links')->where('campaignid','=',$campaignid)->count();
		$chatlink = DB::table('chatlinks')->where('campaignid','=',$campaignid)->count();
		$visitor = DB::table('visitors')->where('campaignid','=',$campaignid)->count();
		$todaysvisitor = DB::table('visitors')->where([['campaignid','=',$campaignid],['created_at','>=',$today]])->count();

		return view('pages.dashboard', ['campaigns' => $campaign,'scripts'=>$script,'keyword'=>$keyword,'models'=>$models,'chatlink'=>$chatlink,'messagelink'=>$messagelink,'visitor'=>$visitor,'todaysvisitor'=>$todaysvisitor]);
    }
}
