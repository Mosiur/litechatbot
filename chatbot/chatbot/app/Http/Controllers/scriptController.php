<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\script;
use App\gallery;
use DB;
use Session;

class scriptController extends Controller{
    public function index(){
        $campaignid=Session::get('user')['campaignid'];
        $script=script::where('campaignid','=',$campaignid)->get();
        $gallery=gallery::where('campaignid','=',$campaignid)->get();
    	return view('pages.script',['script'=>$script,'gallery'=>$gallery]);
    }
    public function insert(Request $request){
        $campaignid=Session::get('user')['campaignid'];
    	$data=$request->except(['_token']);
        $data['campaignid']=$campaignid;

        if ($request->attachment != NULL) {
            $attachment = implode("|", $data['attachment']);
            unset($data['attachment']);
            $data['attachment'] =$attachment;
        }       
        
    	$affected=script::create($data);
		if($affected){
			return redirect()->back()->with('success', 'Record Added successfully!');
		}else{
			return redirect()->back()->with('failed', 'Failed  to Add Record!');
		}
    }
    public function edit(){
        $campaignid=Session::get('user')['campaignid'];
        $id=$_GET['id'];
        $res=script::find($id);
        $attachments = explode("|", $res->attachment);
        $gallery=gallery::where('campaignid','=',$campaignid)->get();
        if(isset($res->attachment)){
            $img =gallery::find($res->attachment);
        }
        ?>
        <input type="id" name="id" value="<?php echo $res->id;?>" hidden>
        <div class="row row-xs align-items-center mg-b-20">
            <div class="col-md-3">
                <label class="form-label mg-b-0">Keyword:</label>
            </div>
            <div class="col-md-9 mg-t-5 mg-md-t-0">
                <textarea class="form-control" required type="text" name="keyword" placeholder="Enter replay message" ><?php echo $res->keyword;?></textarea>
            </div>
        </div>
        <div class="row row-xs align-items-center mg-b-20">
            <div class="col-md-3">
                <label class="form-label mg-b-0">Attachment:</label>
            </div>
            <div class="col-md-9 mg-t-5 mg-md-t-0">
                <?php
                foreach ($gallery as $key => $value) {
                    ?>                    
                    <div class="col-md-2" style="float: left !important;">
                        <label class="ckbox">
                            <input type="checkbox" name="attachment[]" id="attachment" value="<?php echo $value->link;?>" <?php if (in_array($value->link,$attachments)) { echo 'checked';}?>><span><img style="width: 40px;" src="<?php echo $value->link;?>"></span>
                        </label>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="row row-xs align-items-center mg-b-20">
            <div class="col-md-3">
                <label class="form-label mg-b-0">Script:</label>
            </div>
            <div class="col-md-9 mg-t-5 mg-md-t-0">
                <textarea type="text" class="ckeditor" name="up_script" required ><?php echo $res->script;?></textarea>
            </div>
        </div>
        <script>
            CKEDITOR.replace( 'up_script', {
            } );
        </script>
        <div class="row row-xs align-items-center mg-b-20">
            <div class="col-md-3"></div>
            <div class="col-md-9 mg-t-5 mg-md-t-0">
                <div class="col-md-3" style="float: left; margin: 0px; padding: 0px; display: block;">
                    <button class="btn btn-main-primary pd-x-10 mg-r-5 mg-t-5" type="submit">Save Changes</button>
                </div>
                <div class="col-md-2" style="float: left; margin: 0px; padding: 0px; display: block;">
                    <button class="btn btn-dark pd-x-25 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
                </div>
            </div>
        </div>
        <?php
    }
    public function update(Request $request){
        $data=$request->except(['_token']);
        $res=script::find($data['id']);
        $res->keyword=$data['keyword'];
        $res->script=$data['up_script'];

        if ($request->attachment != NULL) {
            $attachment = implode("|", $data['attachment']);
            unset($data['attachment']);
            $res->attachment =$attachment;
        }

        if ($request->attachment == NULL) {
            $res->attachment = $request->attachment;
        }

        $affected=$res->save();
        if($affected){
            return redirect()->back()->with('success', 'Record Updated successfully!');
        }else{
            return redirect()->back()->with('failed', 'Failed  to Update Record!');
        }
    }
    public function delete($id){
        $res=script::find($id);
        if($res->delete()){
            return redirect()->back()->with('success', 'Record Deleted successfully!');
        }else{
            return redirect()->back()->with('failed', 'Failed  to Delete Record!');
        }
    }
}
