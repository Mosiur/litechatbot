<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\item;
use DB;
use Session;
use File;
use Image;
class modelController extends Controller
{
	public function index(){
        $campaignid=Session::get('user')['campaignid'];
        $item=item::where('campaignid','=',$campaignid)->get();
		return view('pages.model',['model'=>$item]);
	}
	public function insert(Request $request){
        $campaignid=Session::get('user')['campaignid'];
		$name=$request->input('name');
        $age=$request->input('age');
		$image=$request->file('profile');//File Name
        if ($request->hasfile('profile')) {

        	$extention=$image->getClientOriginalExtension();//File Extention
        	$newimage_name=time().'.'.$extention;
        	$image_resize = Image::make($image->getRealPath());   
        	$image_resize->resize(255,291);
        	$image_resize->save('assets/uploads/'.$newimage_name);
        }else{
            $newimage_name="";
        }
    	$item = new item;
        $item->campaignid = $campaignid;
    	$item->name = $name;
        $item->age = $age;
    	$item->profile = $newimage_name;
    	$affected=$item->save();

    	if($affected){
    		return redirect()->back()->with('success', 'Record Added successfully!');
    	}else{
    		return redirect()->back()->with('failed', 'Failed  to Add Record!');
    	}
    }
    public function edit(){
    	$id=$_GET['id'];
    	$res=item::find($id);
    	?>
    	<input type="id" name="id" value="<?php echo $res->id;?>" hidden>
    	<div class="row row-xs align-items-center mg-b-20">
    		<div class="col-md-3">
    			<label class="form-label mg-b-0">Name:</label>
    		</div>
    		<div class="col-md-9 mg-t-5 mg-md-t-0">
    			<input name="name" class="form-control" value="<?php echo $res->name;?>" type="text">
    		</div>
    	</div>
        <div class="row row-xs align-items-center mg-b-20">
            <div class="col-md-3">
                <label class="form-label mg-b-0">Age:</label>
            </div>
            <div class="col-md-9 mg-t-5 mg-md-t-0">
                <input name="age" class="form-control" value="<?php echo $res->age;?>" type="text">
            </div>
        </div>
    	<div class="row row-xs align-items-center mg-b-20">
    		<div class="col-md-3">
    			<label class="form-label mg-b-0">Profile Picture:</label>
    		</div>
    		<div class="col-md-9 mg-t-5 mg-md-t-0">
    			<input name="profile" class="form-control" type="file">
    			<img src="<?php echo "assets/uploads/".$res->profile;?>">
    		</div>
    	</div>
    	<div class="row row-xs align-items-center mg-b-20">
    		<div class="col-md-3"></div>
    		<div class="col-md-9 mg-t-5 mg-md-t-0">
    			<div class="col-md-3" style="float: left; margin: 0px; padding: 0px; display: block;">
    				<button class="btn btn-main-primary pd-x-10 mg-r-5 mg-t-5" type="submit">Save Changes</button>
    			</div>
    			<div class="col-md-2" style="float: left; margin: 0px; padding: 0px; display: block;">
    				<button class="btn btn-dark pd-x-25 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
    			</div>
    		</div>
    	</div>
    	<?php
    }
    public function update(Request $request){

    	$item  = new item;
    	$id    = $request->input('id');
    	$name  = $request->input('name');
        $age  = $request->input('age');
		$image = $request->file('profile');//File Name
		$item = item::find($id);
		if($request->hasfile('profile')){
			unlink("assets/uploads/".$item->profile);
	    	$extention=$image->getClientOriginalExtension();//File Extention
	    	$newimage_name=time().'.'.$extention;
	    	$image_resize = Image::make($image->getRealPath());   
	    	$image_resize->resize(255,291);
	    	$image_resize->save('assets/uploads/'.$newimage_name);

	    	
	    	$item->name = $name;
            $item->age = $age;
	    	$item->profile = $newimage_name;
	    	$affected=$item->save();
	    	if($affected){
	    		return redirect()->back()->with('success', 'Record Updated successfully!');
	    	}else{
	    		return redirect()->back()->with('failed', 'Failed  to Update Record!');
	    	}
	    }else{
	    	$item->name = $name;
            $item->age = $age;
	    	$affected=$item->save();
	    	if($affected){
	    		return redirect()->back()->with('success', 'Record Updated successfully!');
	    	}else{
	    		return redirect()->back()->with('failed', 'Failed  to Update Record!');
	    	}
	    }
	}
	public function delete($id){
		$res=item::find($id);
        if (File::exists("assets/uploads/".$res->profile)) { 
            unlink("assets/uploads/".$res->profile);
        }
		if($res->delete()){
			return redirect()->back()->with('success', 'Record Deleted successfully!');
		}else{
			return redirect()->back()->with('failed', 'Failed  to Delete Record!');
		}
	}
}
