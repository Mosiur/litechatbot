<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\keyword;
use DB;
use Session;

class keywordController extends Controller
{
	public function index(){
		$campaignid=Session::get('user')['campaignid'];
        $keyword=keyword::where('campaignid','=',$campaignid)->get();
		return view('pages.keyword',['keyword'=>$keyword]);
	}
	public function insert(Request $request){
		$campaignid=Session::get('user')['campaignid'];
		$data=$request->except(['_token']);
		$data['campaignid']=$campaignid;
		$affected=keyword::create($data);
		if($affected){
			return redirect()->back()->with('success', 'Record Added successfully!');
		}else{
			return redirect()->back()->with('failed', 'Failed  to Add Record!');
		}
	}
	public function edit(){
		$id=$_GET['id'];
		$res=keyword::find($id);
		?>
		<input type="id" name="id" value="<?php echo $res->id;?>" hidden>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-3">
				<label class="form-label mg-b-0">Keyword:</label>
			</div>
			<div class="col-md-9 mg-t-5 mg-md-t-0">
				<textarea type="text" class="ckeditor" name="up_keyword" required ><?php echo $res->keyword;?></textarea>
			</div>
		</div>
		<script>
			CKEDITOR.replace( 'up_keyword', {
			} );
		</script>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-3"></div>
			<div class="col-md-9 mg-t-5 mg-md-t-0">
				<div class="col-md-3" style="float: left; margin: 0px; padding: 0px; display: block;">
					<button class="btn btn-main-primary pd-x-10 mg-r-5 mg-t-5" type="submit">Save Changes</button>
				</div>
				<div class="col-md-2" style="float: left; margin: 0px; padding: 0px; display: block;">
					<button class="btn btn-dark pd-x-25 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
				</div>
			</div>
		</div>
		<?php
	}
	public function update(Request $request){
		$data=$request->except(['_token']);
		$res=keyword::find($data['id']);
		$res->keyword=$data['up_keyword'];
		$affected=$res->save();
		if($affected){
			return redirect()->back()->with('success', 'Record Updated successfully!');
		}else{
			return redirect()->back()->with('failed', 'Failed  to Update Record!');
		}
	}
	public function delete($id){
		$res=keyword::find($id);
		if($res->delete()){
			return redirect()->back()->with('success', 'Record Deleted successfully!');
		}else{
			return redirect()->back()->with('failed', 'Failed  to Delete Record!');
		}
	}
}
