<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\visitor;
use App\conversation;
use DB;
use Session;

class conversationController extends Controller{
	
	public function index(){
		$campaignid=Session::get('user')['campaignid'];
		$visitor = visitor::where('campaignid','=',$campaignid)->orderBy('id','DESC')->get();
		return view('pages.visitor', ['visitors' => $visitor]);
	}

	public function conversation(){
		$campaignid=Session::get('user')['campaignid'];
		$campaign = DB::table('campaign')->select('name')->where('id','=',$campaignid)->first();
		$user = DB::table('chat')->select('sender')->distinct('sender')->where('campaign_id','=',$campaignid)->get();
		foreach ($user as $key => $value) {
			$script = DB::table('chat')->select('replay_script')->where([['campaign_id','=',$campaignid],['sender','=',$value->sender]])->count();
			$location = DB::table('visitors')->select('city','country','created_at')->where([['campaignid','=',$campaignid],['user','=',$value->sender]])->first();
			if (!empty($location->city)) {
				$userdata[]=['user'=>$value->sender,'campaign'=>$campaign->name,'city'=>$location->city,'Country'=>$location->country,'script'=>$script,'created_at'=>$location->created_at];
			}

		}
		return view('pages.conversation', ['userdata' => $userdata]);
	}

	public function truckuser(){
		$arr_ip = geoip()->getLocation($_SERVER['REMOTE_ADDR']);
		dd($arr_ip);
		print_r($arr_ip);
        echo $arr_ip->country; // get a country
        echo $arr_ip->currency; // get a currency
    }
    public function getusersysteminfo(){
    	$getip = UserSystemInfoHelper::get_ip();
    	$getbrowser = UserSystemInfoHelper::get_browsers();
    	$getdevice = UserSystemInfoHelper::get_device();
    	$getos = UserSystemInfoHelper::get_os();
    	echo "<center>$getip <br> $getdevice <br> $getbrowser <br> $getos</center>";
    }
    public function view_conversation(){
        ?>
        <div class="col-md-12 chat_body">
        <?php
    	$userid = $_GET['userid'];
    	$campaignid=Session::get('user')['campaignid'];
    	$data = DB::table('chat')->where([['sender','=',$userid],['campaign_id','=',$campaignid]])->get();
    	foreach ($data as $key => $value) {
    		$model = DB::table('items')->select('name','profile')->WHERE([['id','=',$value->receiver],['campaignid','=',$value->campaign_id]])->first();

            $sender_name = $model->name;
            $sender_profile = $model->profile;
            $sender_message = $value->replay_script;
            $screated_at = $value->updated_at;
            $sender_attachment = $value->replay_attachment;

            $receiver_name = "Visitor";
            $receiver_message = $value->script;
            $rcreated_at = $value->created_at;
            $receiver_attachment = $value->attachment;
            if($receiver_message != NULL){ ?>
                <div class="col-md-12 receiver">
                    <div class="col-md-1 profile_pic">
                        <img src="http://litechatbot.com/chat/uploads/sender.png">
                    </div>
                    <div class="col-md-8">
                        <p style=" font-size: 14px; font-family: sans-serif; margin-top: 0px; margin-bottom: 0px; font-weight: bold;"><?php echo $receiver_name?><span style ="font-weight: bold; font-size: 11px; font-family: sans-serif;"><br><?php echo $rcreated_at;?></span>
                        </p>
                        <div class="message"><p><?php echo $receiver_message;?></p></div>
                            <?php
                            if($receiver_attachment != NULL){?>
                                <div class="col-md-12 attachment">
                                 <img src="http://litechatbot.com/chatbot/<?php echo $receiver_attachment;?>">
                             </div>
                             <?php
                            }
                         ?>
                     </div>
                </div>
             <?php
            }
            if($sender_message != NULL){ ?>
                <div class="col-md-12 sender">
                    <div class="col-md-1" style="float: right;">
                        <img style="height: 30px;width: 30px; float: right; border-radius: 25px;" src="http://litechatbot.com/chatbot/assets/uploads/<?php echo $sender_profile;?>">
                    </div>
                    <div class="col-md-8">
                        <p style=" font-size: 14px; font-family: sans-serif; margin-top: 0px; margin-bottom: 0px; font-weight: bold;"><?php echo $sender_name?><span style ="font-weight: bold; font-size: 11px; font-family: sans-serif;"><br><?php echo $screated_at;?></span>
                        </p>
                        <div class="message">
                            <?php echo $sender_message;?>
                        </div>
                        <?php
                        if($sender_attachment != NULL){ ?>
                            <div class="col-md-12 attachment">
                                <img src="uploads/<?php echo $sender_attachment;?>">
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            <?php
            }   
        }
        ?>
      </div>
    <?php
    }

    public function delete($userid){
    	$res=visitor::find($userid);
    	$affected=DB::table('chat')->where($userid)->delete();

		if($res->delete() && $affected){

			return redirect()->back()->with('success', 'Record Deleted successfully!');
		}else{
			return redirect()->back()->with('failed', 'Failed  to Delete Record!');
		}
    }
}
