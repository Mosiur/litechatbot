<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\UserSystemInfoHelper;
use DB;
use Session;

class reportController extends Controller{
	public function index(){
		$campaignid=Session::get('user')['campaignid'];
		$campaigns = DB::table('campaign')->where('id','=',$campaignid)->get();
		return view('pages.report', ['data' => $campaigns]);
	}
	public function searchreport(Request $request){
		$campaignid= $request->input('camping');
		$camdata=DB::table('campaign')->where('id','=',$campaignid)->get();
		$from = $request->input('from');
		$to = $request->input('to');
		// $conversation=DB::table()->where('conversation')->where([['campaignid','=',$campaignid],['created_at','>=',$from],['created_at','<=',$to]])->count();
		?>
		<div class="col-12">
			<div class="card mg-b-20">
				<div class="card-body">
					<div class="main-content-label mg-b-5">
						Report Result
					</div>
					<div class="table-responsive">
						<table class="table text-md-nowrap" id="example1">
							<thead>
								<tr>
									<th class="wd-15p border-bottom-0">Campaign</th>
									<th class="wd-15p border-bottom-0">Conversation</th>
									<th class="wd-20p border-bottom-0">Script</th>
									<th class="wd-15p border-bottom-0">Link1</th>
									<th class="wd-10p border-bottom-0">Link2</th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach ($camdata as $key => $row) {
									$conversation =DB::table('conversation')
									->select(
										DB::raw('count(script) as script'),
										DB::raw('count(keyword) as keyword'),
										DB::raw('count(link1) as link1'),
										DB::raw('count(link2) as link2')
									)
									->where([
										['campaignid','=',$campaignid],
										['created_at','>=',$from],
										['created_at','<=',$to]
									])->get();
									foreach ($conversation as $key => $value) {
										?>
										<tr>
											<td><?php echo $row->name;?></td>
											<td><?php echo $value->script;?></td>
											<td><?php echo $value->keyword?></td>
											<td><?php echo $value->link1?></td>
											<td><?php echo $value->link2?></td>
										</tr>
										<?php
									}
								}
								?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
}
