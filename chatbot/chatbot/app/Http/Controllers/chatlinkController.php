<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\chatlink;
use DB;
use Session;

class chatlinkController extends Controller
{
    public function index(){
		$link=chatlink::all();
		return view('pages.chatlink',['chatlink'=>$link]);
	}
	public function insert(Request $request){
		$campaignid=Session::get('user')['campaignid'];
		$data=$request->except(['_token']);
		$data['campaignid']=$campaignid;
		
		$affected=chatlink::create($data);
		if($affected){
			return redirect()->back()->with('success', 'Record Added successfully!');
		}else{
			return redirect()->back()->with('failed', 'Failed  to Add Record!');
		}
	}
	public function edit(){
		$id=$_GET['id'];
		$res=chatlink::find($id);
		?>
		<input type="id" name="id" value="<?php echo $res->id;?>" hidden>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-3">
				<label class="form-label mg-b-0">Link:</label>
			</div>
			<div class="col-md-9 mg-t-5 mg-md-t-0">
				<input name="chat_link" class="form-control" placeholder="Enter Link address" type="text" value="<?php echo $res->chat_link;?>">
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-3"></div>
			<div class="col-md-9 mg-t-5 mg-md-t-0">
				<div class="col-md-3" style="float: left; margin: 0px; padding: 0px; display: block;">
					<button class="btn btn-main-primary pd-x-10 mg-r-5 mg-t-5" type="submit">Save Changes</button>
				</div>
				<div class="col-md-2" style="float: left; margin: 0px; padding: 0px; display: block;">
					<button class="btn btn-dark pd-x-25 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
				</div>
			</div>
		</div>
		<?php
	}
	public function update(Request $request){
		$data=$request->except(['_token']);
		$res=chatlink::find($data['id']);
		$res->chat_link=$data['chat_link'];
		$affected=$res->save();
		if($affected){
			return redirect()->back()->with('success', 'Record Updated successfully!');
		}else{
			return redirect()->back()->with('failed', 'Failed  to Update Record!');
		}
	}
	public function delete($id){
		$res=chatlink::find($id);
		if($res->delete()){
			return redirect()->back()->with('success', 'Record Deleted successfully!');
		}else{
			return redirect()->back()->with('failed', 'Failed  to Delete Record!');
		}
	}
}
