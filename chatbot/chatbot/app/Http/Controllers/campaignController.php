<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class campaignController extends Controller
{
    public function index(){
    	$campaignid=Session::get('user')['campaignid'];
		$campaign = DB::table('campaign')->where('id','=',$campaignid)->get();
		$script   = DB::table('scripts')->where('campaignid','=',$campaignid)->count();
		$keyword  = DB::table('keywords')->where('campaignid','=',$campaignid)->count();
		$models    = DB::table('items')->where('campaignid','=',$campaignid)->get();
		$messagelink = DB::table('links')->where('campaignid','=',$campaignid)->get();
		$chatlink = DB::table('chatlinks')->where('campaignid','=',$campaignid)->get();

		return view('pages.campaign', ['campaigns' => $campaign,'scripts'=>$script,'keyword'=>$keyword,'models'=>$models,'chatlink'=>$chatlink,'messagelink'=>$messagelink]);
	}
	public function insert(Request $request){
		$campaignid=Session::get('user')['campaignid'];
		$campaign = DB::table('campaign')->where('id','=',$campaignid)->get();
		if ($campaign->isEmpty()) {
			$campaign_name = $request->input('name');
			$affected=DB::table('campaign')->insert(['name'=>$campaign_name]);
			if($affected){
				return redirect('/campaign')->with('success', 'Record Added successfully!');
			}else{
				return redirect('/campaign')->with('failed', 'Failed  to Add Record!');
			}
		}else{
			return redirect()->back()->with('failed', 'Campaign already created for you... try for sub campaign!');
		}
	}

	public function edit(){
		$id = $_GET['id'];
		$data=DB::table('campaign')->where('id','=',$id)->first();
		?>
		<div class="pd-30 pd-sm-40 bg-light">
			<input type="text" name="id" value="<?php echo $id;?>" hidden>
			<div class="row row-xs align-items-center mg-b-20">
				<div class="col-md-4">
					<label class="form-label mg-b-0">Camping Name</label>
				</div>
				<div class="col-md-8 mg-t-5 mg-md-t-0">
					<input name="name" class="form-control" value="<?php echo $data->name;?>" type="text">
				</div>
			</div>
			<div class="row row-xs align-items-center mg-b-20">
				<div class="col-md-4"></div>
				<div class="col-md-8 mg-t-5 mg-md-t-0">
					<div class="col-md-7" style="float: left; margin: 0px; padding: 0px; display: block;">
						<button class="btn btn-main-primary pd-x-25 mg-r-5 mg-t-5" type="submit">Save Changes</button>
					</div>
					<div class="col-md-3" style="float: left; margin: 0px; padding: 0px; display: block;">
						<button class="btn btn-dark pd-x-25 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	public function updt_campaign( Request $request){
		$id =  $request->input('id');
		$campaign_name = $request->input('name');
		if ($campaign_name != NULL) {
			$affected=DB::table('campaign')->where('id','=',$id)->update(['name'=>$campaign_name]);
			if($affected){
				return redirect('/campaign')->with('success', 'Record Updated successfully!');
			}else{
				return redirect('/campaign')->with('failed', 'Failed  to update Record!');
			}
		}
	}
	public function delete($id){
		$affected = DB::table('campaign')->where('id','=',$id)->delete();
		if($affected){
            return redirect()->back()->with('success', 'Record Deleted successfully!');
        }else{
            return redirect()->back()->with('failed', 'Failed to Delete Rcord..!');
        }
	}
}
