<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\visitor;
use App\script;
use DB;
use Session;
use File;
use Image;

class chatController extends Controller{
	public function index(){
		$campaignid=Session::get('user')['campaignid'];
		return  view('pages.chat');
	}
	// public function __construct() {
	// 	$visitor = geoip()->getLocation($_SERVER['REMOTE_ADDR']);
	// 	dd($visitor);
	// 	visitor::create($visitor);
	// }

	public function conversation($campaignid){

		$data = DB::table('chat')->where('campaign_id','=',$campaignid)->get();
		foreach ($data as $key => $value) {
			if ($value->replay_script == NULL) {
				$script=script::where('keyword','LIKE',"%{$value->script}%")->get();
				foreach ($script as $key => $row) {

					// modify script
					$array_script = explode('%', $row->script);
					
					// dd($array_script);

					$script_replay = '';
					foreach ($array_script as $key => $values) {
						if (strpos($values, '|') !== false) {
							$keyword = explode("|", $values);
							$selected_key = $keyword[array_rand($keyword)];
							$script_replay .= $selected_key;

						}else{
							$script_replay .= $values;
						}
					}
					// echo $script_replay;
					// var_dump($script_replay);
					// dd($row->script,$script_replay);
					$affected = DB::table('chat')->where('id','=',$value->id)->update([
						'replay_script'=>$script_replay,
						'replay_attachment'=>$row->attachment
					]);
					if($affected){
						echo 'Record Updated successfully!';
					}else{
						echo'failed', 'Failed  to update Record!';
					}
				}
			}
		}
	}


}
