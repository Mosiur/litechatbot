<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class chatlink extends Model
{
    protected $fillable = [
        'chat_link',
        'campaignid'

    ];
}
