<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class script extends Model
{
    protected $fillable = [
    	'keyword',
        'script',
        'campaignid',
        'attachment'
    ];
}
