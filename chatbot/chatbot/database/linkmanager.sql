-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2020 at 03:40 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mine`
--

-- --------------------------------------------------------

--
-- Table structure for table `linkmanager`
--

CREATE TABLE `linkmanager` (
  `id` int(100) NOT NULL,
  `campaignid` int(250) NOT NULL,
  `link` varchar(250) NOT NULL,
  `token` varchar(250) NOT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `linkmanager`
--

INSERT INTO `linkmanager` (`id`, `campaignid`, `link`, `token`, `created_at`) VALUES
(2, 1, 'http://localhost/phpmyadmin/sql.php?db=mine&table=linkmanager&pos=0', 'localhosts', '2020-06-07 01:37:38.192889'),
(3, 1, 'https://fontawesome.com/icons/at?style=solid', 'Font Awesome', '2020-06-07 01:37:38.192889'),
(4, 1, 'http://localhost/MOSIUR/trust_it/mail_responder/public/', 'mail responder', '2020-06-07 01:37:38.192889');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `linkmanager`
--
ALTER TABLE `linkmanager`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `linkmanager`
--
ALTER TABLE `linkmanager`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
